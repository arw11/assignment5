For question 1 part d):
	 type " g++ q1b.cpp -o q1b" to compile
	 type "./q1b" to run
	 -->This is for the trapezoidal rule. I input 0.000001 for the relative accuracy to yield an integral of 0.995322 in 10 steps. 
	 
	 type " g++ q1c.cpp -o q1c" to compile
	 type "./q1c" to run
	 -->This is for Simpson's rule. I input 0.000001 for the relative accuracy to yield an integral of 0.995322 in 6 steps.
