//This progrmms numerically integrates a given function with the extended trapezoidal rule
#include <iostream>
#include <cmath>

using namespace std;

double f(double x)
{
	const double pi = acos(-1.0);
	return (2.0/sqrt(pi))*exp(-(x*x));	
}
int main()
{	
	double eps; //eps is the relative accuracy required
	cout << "Please enter your relative accuracy: ";
	cin >> eps;
	double h,a,b,c,I1,I2,S; //h is the step difference, a,b are the limits of integration, I1 and I2 are the iterations of each integral, S is I1 without the prefactor of h
	a=0.0; b=2.0;
	int n=2; //Two iterations are passed before the while loop
	h = b-a;
	S =0.5*(f(a)+f(b));
	I1 = h*S;

	c = (b + a)/2;
	h = c - a;
	S = S + f(c);
	I2 = h*S;

	int z = 3; //The function has been evaluated 3 times so far

	while( abs((I2-I1)/I1) > eps)
	{  
		c =(c+a)/2; //take the midpoint between c and a
		h = c-a; //define the new step difference
		for( int i=0; i < pow(2.0,double(n)-1.0); i++) //There are 2^(n-1) gaps to fill in each iteration 
			{	
			   S = S + f(c+(2*i*h)); //The gap between unfilled points is 2*(integer multiple of h)
			  z++; //Add one to the function evaluation counter		
			}
		I1 = I2;
		I2 = h*S;
		n++;

	}
	cout <<"The integral is: "<<  I2 << endl;
	cout <<"It took " << n << " steps to reach a relative accuracy of " << eps << endl;
	cout <<"The function has been evaluated "<< z <<" times." << endl;
	return 0;
}					
